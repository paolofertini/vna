﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Framework.Aggregates;

namespace VNA.CQRS.Server.Archive.Framework.Infrastructure
{
    public interface IKeySession<K>
    {
        void Add<T>(T aggregate) where T : KeyAggregateRoot<K>;
        T Get<T>(K id, int? expectedVersion = null) where T :KeyAggregateRoot<K>;
        void Commit();
    }
}
