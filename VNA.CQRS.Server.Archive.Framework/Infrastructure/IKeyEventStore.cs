﻿using CQRSlite.Events;
using EventStore.ClientAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Framework.Events;

namespace VNA.CQRS.Server.Archive.Framework.Infrastructure
{
    public interface IKeyEventStore<T>
    {
        void Save(IKeyEvent<T> @event);
        IEnumerable<IKeyEvent<T>> Get(T aggregateId, long? fromVersion);
    }

    public interface IKeyEventStoreFactory<T>
    {
        IKeyEventStore<T> Create(IEventStoreConnection connection);
        void Release(IKeyEventStore<T> instance);
    }
}
