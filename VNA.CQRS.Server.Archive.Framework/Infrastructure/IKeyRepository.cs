﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Framework.Aggregates;

namespace VNA.CQRS.Server.Archive.Framework.Infrastructure
{
    public interface IKeyRepository<K>
    {
        void Save<T>(T aggregate, long? expectedVersion = null) where T : KeyAggregateRoot<K>;
        T Get<T>(K aggregateId) where T : KeyAggregateRoot<K>;
    }
}
