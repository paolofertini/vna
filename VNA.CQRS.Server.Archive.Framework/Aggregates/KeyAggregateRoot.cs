﻿using CQRSlite.Domain.Exception;
using CQRSlite.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Framework.Events;

namespace VNA.CQRS.Server.Archive.Framework.Aggregates
{
    public abstract class KeyAggregateRoot<T> : IKeyAggregateRoot<T>
    {
        private readonly List<IKeyEvent<T>> _changes = new List<IKeyEvent<T>>();

        public T Id { get; protected set; }
        public long Version { get; protected set; }

        public IEnumerable<IKeyEvent<T>> GetUncommittedChanges()
        {
            lock (_changes)
            {
                return _changes.ToArray();
            }
        }

        public void MarkChangesAsCommitted()
        {
            lock (_changes)
            {
                Version = Version + _changes.Count;
                _changes.Clear();
            }
        }

        public void LoadFromHistory(IEnumerable<IKeyEvent<T>> history)
        {
            foreach (var e in history)
            {
                if (e.Version != Version + 1)
                    throw new EventsOutOfOrderException<T>(e.Id);
                ApplyChange(e, false);
            }
        }

        protected void ApplyChange(IKeyEvent<T> @event)
        {
            ApplyChange(@event, true);
        }

        private void ApplyChange(IKeyEvent<T> @event, bool isNew)
        {
            lock (_changes)
            {
                //(this as dynamic).Apply(@event);

                if (isNew)
                {
                    _changes.Add(@event);
                }
                else
                {
                    Id = @event.Id;
                    Version = @event.Version;
                    //Version++;
                }
            }
        }
    }
}
