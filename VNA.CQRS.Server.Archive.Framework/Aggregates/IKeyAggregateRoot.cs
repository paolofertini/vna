﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Framework.Aggregates
{
    public interface IKeyAggregateRoot<T>
    {
        T Id { get; }
    }
}
