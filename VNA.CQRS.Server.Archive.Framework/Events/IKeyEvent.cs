﻿using CQRSlite.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using VNA.CQRS.Server.Archive.Domain.Commands;
//using VNA.CQRS.Server.Archive.Domain.Values;

namespace VNA.CQRS.Server.Archive.Framework.Events
{
    public interface IKeyEvent<T> : IMessage
    {
        T Id { get; set; }
        long Version { get; set; }
        DateTimeOffset TimeStamp { get; set; }

        //Header Header { get; set; }
        //JSonObj Metadata { get; set; }
    }
}
