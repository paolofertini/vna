﻿using CQRSlite.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using VNA.CQRS.Server.Archive.Domain.Values;

namespace VNA.CQRS.Server.Archive.Framework.Commands
{
    public interface IKeyCommand<K> : IMessage
    {
        K Id { get; set; }
        int ExpectedVersion { get; set; }
    }
}
