﻿using CQRSlite.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Framework.Commands
{
    public interface IKeyCommandHandler<T, K> : IHandler<T> where T : IKeyCommand<K>
    {
    }
}
