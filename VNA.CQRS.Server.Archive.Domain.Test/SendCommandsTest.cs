﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Castle.Windsor;
using VNA.CQRS.Server.Archive.Domain.IOC;
using VNA.CQRS.Server.Archive.Domain.Host;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.Domain.Commands;
using System.ServiceModel;
using Castle.Facilities.WcfIntegration;
using VNA.CQRS.Server.Archive.Domain.Infrastructure;
using System.Threading.Tasks;
using System.Linq;
using System.Reflection;
using VNA.CQRS.Server.Archive.Domain.Client;
using Nito.AsyncEx.UnitTests;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace VNA.CQRS.Server.Archive.Domain.Test
{
    [TestClass]
    public class SendCommandsTest
    {
        //private WindsorContainer container;
        //private ServiceHostBase dealingServiceHost;
        private ArchiveServiceInvoker archiveServiceInvoker;

        [TestInitialize]
        public void Setup()
        {
            //container = new WindsorContainer();

            //container.Install(
            //      new DomainInstaller(new VnaLifestyleApplier()));
            //container.CheckForPotentiallyMisconfiguredComponents();

            ////var readModelRespository = container.Resolve<IReadModelRepository>();
            ////readModelRespository.CreateFreshDb().Wait();

            //CreateServiceHost<IArchiveService>(ref dealingServiceHost, "ArchiveService");

            //archiveServiceInvoker = new ArchiveServiceInvoker(new NetHttpBinding(), new EndpointAddress(@"net.tcp://localhost:63105/ArchiveService/"));
            archiveServiceInvoker = new ArchiveServiceInvoker();
        }

        [TestCleanup]
        public void Cleanup()
        {
            //container.Dispose();
        }

        [TestMethod]
        //[ExpectedException(typeof(System.ServiceModel.FaultException<System.ServiceModel.ExceptionDetail>))]
        public async Task SendANewDocumentAndStoreToRepository()
        {
            AggregateId documentId = new AggregateId()
            {
                Id = "1.2.826.0.1.3680043.2.97.19529.21573.3025147141.1802070949495780"
            };

            //var testService = container.Kernel.Resolve(container.Kernel.GetHandler(typeof(IArchiveService)).ComponentModel.Services.First());
            //var methods = container.Kernel.GetHandler(typeof(IArchiveService)).ComponentModel.Services.First().GetMethod("SendCommandAsync");

            //var mets = (from m in container.Kernel.Resolve(container.Kernel.GetHandler(typeof(IArchiveService)).ComponentModel.Services)
            //        .GetMethods(BindingFlags.Public | BindingFlags.Instance)
            // select m).FirstOrDefault();

            //IArchiveService testService = container.Kernel.Resolve<IArchiveService>();


            CreateDocumentCommand document = new CreateDocumentCommand()
                {
                    Id = documentId,
                    Header = new Header()
                    {
                        Patient = new Patient() { ID = "PID", Surname = "TEST", Name = "TEST", BirthDate = DateTime.Today, IDIssuer = "VNA", Sex = Patient.Gender.Unkwnown },
                        Info = new DocumentInfo() { Department = new Department()
                                                        {   
                                                            Name = "Oncologia",
                                                            SendingApplication = "RM Philips"
                                                        },
                                                     Data = DateTime.Today,
                                                     Type = "MR" }
                    },                    
                };

            using (StreamReader r = new StreamReader(@"C:\TMP\KOS.json"))
            {
                string json = r.ReadToEnd();
                //dynamic items = JsonConvert.DeserializeObject(json);

                document.Metadata = new JSonObj()
                {
                    ValueX = json 
                };
            }

            await archiveServiceInvoker.CallService(service =>
                   service.SendCommandAsync(document));

            //Assert.Fail("ERRORE");

            //methods.Invoke(testService, new[] { command });

            //var res = await testService.SendCommandAsync(command);

            //Task.Run(() =>
            //{
            //    testService.SendCommandAsync(command);
            //}).GetAwaiter().GetResult();
        }

        //private void CreateServiceHost<T>(ref ServiceHostBase host, string message)
        //{
        //    host = new DefaultServiceHostFactory(container.Kernel)
        //        .CreateServiceHost(typeof(T).AssemblyQualifiedName, new Uri[0]);
        //}
    }
}
