﻿using CQRSlite.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CQRSlite.Messages;
using CQRSlite.Domain;
using VNA.CQRS.Server.Archive.Domain.Aggregates;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.Domain.Infrastructure;
using VNA.CQRS.Server.Archive.Framework.Commands;
using VNA.CQRS.Server.Archive.Framework.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VNA.CQRS.Server.Archive.Domain.Commands
{
    public class ArchiveCommandsHandlers:    IKeyCommandHandler<CreateDocumentCommand, AggregateId>,
                                             IKeyCommandHandler<CreateOrderCommand, AggregateId>
    {
        private readonly IKeySession<AggregateId> _session_tracking;

        public ArchiveCommandsHandlers(IKeySession<AggregateId> session)
        {
            _session_tracking = session;
        }

        public void Handle(CreateOrderCommand message)
        {
            throw new NotImplementedException();
        }

        public void Handle(CreateDocumentCommand message)
        {
            var item = new Document(message.Id, message.Header, JsonConvert.DeserializeObject<JObject>(message.Metadata.ValueX));

            _session_tracking.Add(item);
            _session_tracking.Commit();
        }
    }
}
