﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Values;

namespace VNA.CQRS.Server.Archive.Domain.Commands
{

    [DataContract]
    public class CreateDocumentCommand : KeyCommand
    {
        [DataMember]
        public Header Header { get; set; }

        [DataMember]
        public JSonObj Metadata { get; set; }
    }
}
