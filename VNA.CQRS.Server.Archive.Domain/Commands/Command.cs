﻿using CQRSlite.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.Framework.Commands;

namespace VNA.CQRS.Server.Archive.Domain.Commands
{
    [DataContract]
    [KnownType(typeof(CreateOrderCommand))]
    [KnownType(typeof(CreateDocumentCommand))]
    public abstract class KeyCommand : IKeyCommand<AggregateId>
    {
        [DataMember]
        public AggregateId Id { get; set; }

        [DataMember]
        public int ExpectedVersion { get; set; }
    }
}
