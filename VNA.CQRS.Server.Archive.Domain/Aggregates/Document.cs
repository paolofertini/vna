﻿using CQRSlite.Domain;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Commands;
using VNA.CQRS.Server.Archive.Domain.Events;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.Framework.Aggregates;

namespace VNA.CQRS.Server.Archive.Domain.Aggregates
{
    public class Document : KeyAggregateRoot<AggregateId>
    {
        private Header Header;
        private JObject Metadata;

        private Document() { }
        public Document(AggregateId id, Header header, JObject metadata)
        {
            DocumentCreatedEvent<AggregateId> document = new DocumentCreatedEvent<AggregateId>(id, header, metadata);
            Apply( document );

            base.Id = id;
            base.ApplyChange(document);
        }

        private void Apply(DocumentCreatedEvent<AggregateId> e)
        {
            base.Version = e.Version;

            Header = e.Header;
            Metadata = e.Metadata;
        }
    }
}
