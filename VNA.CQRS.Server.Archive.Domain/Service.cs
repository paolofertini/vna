﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Host;

namespace VNA.CQRS.Server.Archive.Domain
{
    partial class Service : ServiceBase
    {
        private ArchiveServiceRunner archiveServiceRunner;

        public Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.archiveServiceRunner = new ArchiveServiceRunner();
            this.archiveServiceRunner.Start();
        }

        protected override void OnStop()
        {
            if (this.archiveServiceRunner != null)
                this.archiveServiceRunner.Stop();
        }
    }
}
