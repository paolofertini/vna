﻿using Castle.Facilities.WcfIntegration;
using Castle.Windsor;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Infrastructure;
using VNA.CQRS.Server.Archive.Domain.IOC;
using VNA.CQRS.Server.Archive.ReadModel;

namespace VNA.CQRS.Server.Archive.Domain.Host
{
    class ArchiveServiceRunner
    {
        private ServiceHostBase dealingServiceHost;
        private ServiceHostBase dealingSearchServiceHost;
        private WindsorContainer container = new WindsorContainer();
        private Logger logger = LogManager.GetLogger("ArchiveServiceRunner");

        public void Start()
        {

            logger.Info("Starting VNA Server ArchiveService");

            try
            {
                container.Install(
                    new DomainInstaller(new VnaLifestyleApplier()));
                container.CheckForPotentiallyMisconfiguredComponents();

                var readModelRespository = container.Resolve<IReadModelRepository>();
                //readModelRespository.CreateFreshDb().Wait();

                CreateServiceHost<IArchiveService>(ref dealingServiceHost, "ArchiveService");


                //hook up unhandled exception listeners
                WcfExceptionHandler wcfExceptionHandler = new WcfExceptionHandler();
                ExceptionHandler.AsynchronousThreadExceptionHandler = wcfExceptionHandler;
                ExceptionHandler.TransportExceptionHandler = wcfExceptionHandler;
            }
            catch (Exception)
            {
                throw;
            }
        }


        private void CreateServiceHost<T>(ref ServiceHostBase host, string message)
        {
            try
            {
                host = new DefaultServiceHostFactory(container.Kernel)
                    .CreateServiceHost(typeof(T).AssemblyQualifiedName, new Uri[0]);

                // Hook on to the service host faulted events.
                host.Faulted += new WeakEventHandler<EventArgs>(OnServiceFaulted).Handler;

                // Open the ServiceHostBase to create listeners and start listening for messages.
                host.Open();
                logger.Info(string.Format("VNA Server Domain {0} Started", message));
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }


        public void OnServiceFaulted(object sender, EventArgs e)
        {
            logger.Error("VNA Server ArchiveService faulted");
            Environment.Exit(-1);
        }

        public void Stop()
        {
            try
            {
                dealingServiceHost.Close();
                dealingSearchServiceHost.Close();
            }
            catch (Exception)
            {
                dealingServiceHost.Abort();
                dealingSearchServiceHost.Abort();
                throw;
            }
        }
    }
}
