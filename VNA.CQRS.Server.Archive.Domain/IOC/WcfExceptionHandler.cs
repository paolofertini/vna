﻿using System;
using System.ServiceModel.Dispatcher;

using NLog;

namespace VNA.CQRS.Server.Archive.Domain.IOC
{
    public class WcfExceptionHandler : ExceptionHandler
    {
        private Logger logger = LogManager.GetLogger("WcfExceptionHandler");

        public override bool HandleException(Exception exception)
        {
            logger.Error("VNA.CQRS.Server.Archive.Domain.Host : {0}", exception.StackTrace);
            return true;
        }
    }
}
