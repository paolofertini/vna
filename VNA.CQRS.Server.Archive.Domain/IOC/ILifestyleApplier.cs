﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Registration.Lifestyle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Domain.IOC
{
    public interface ILifestyleApplier
    {
        ComponentRegistration<T> ApplyLifeStyle<T>(LifestyleGroup<T> component) where T : class;
    }

    public class VnaLifestyleApplier : ILifestyleApplier
    {
        public ComponentRegistration<T> ApplyLifeStyle<T>(LifestyleGroup<T> component) where T : class
        {
            return component.PerWcfSession();
        }
    }

    public class TransientLifestyleApplier : ILifestyleApplier
    {
        public ComponentRegistration<T> ApplyLifeStyle<T>(LifestyleGroup<T> component) where T : class
        {
            return component.Transient;
        }
    }
}
