﻿using Castle.Facilities.TypedFactory;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using EventStore.ClientAPI;
using System.Net;
using VNA.CQRS.Server.Archive.Domain.Commands;
using VNA.CQRS.Server.Archive.Domain.Infrastructure;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.Framework.Infrastructure;
using VNA.CQRS.Server.Archive.ReadModel;

namespace VNA.CQRS.Server.Archive.Domain.IOC
{
    public class DomainInstaller : IWindsorInstaller
    {
        private readonly ILifestyleApplier lifestyleApplier;

        public DomainInstaller(ILifestyleApplier lifestyleApplier)
        {
            this.lifestyleApplier = lifestyleApplier;
        }

        public void Install(Castle.Windsor.IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            container.Kernel.Resolver.AddSubResolver(new ArrayResolver(container.Kernel, true));

            // PerCall setup
            container.AddFacility<WcfFacility>()
                .AddFacility<TypedFactoryFacility>()
                .Register(

                    //WCF services
                    Component.For<IArchiveService>()
                        .ImplementedBy<ArchiveService>()
                        .LifeStyle.ApplyLifeStyle(lifestyleApplier),

                    //CQRSLite stuff
                    Component.For<ArchiveCommandsHandlers>().LifeStyle.ApplyLifeStyle(lifestyleApplier),
                    //Component.For<IEventPublisher>().ImplementedBy<BusEventPublisher>().LifeStyle.Singleton,
                    //Component.For<IInterProcessBus>().ImplementedBy<InterProcessBus>().LifeStyle.Singleton,
                    Component.For<IKeySession<AggregateId>>().ImplementedBy<SessionTracking>().LifeStyle.ApplyLifeStyle(lifestyleApplier),
                    Component.For<IKeyEventStore<AggregateId>>().ImplementedBy<ArchiveEventStore>().LifestyleTransient(),
                    Component.For<IKeyEventStoreFactory<AggregateId>>().AsFactory(),
                    Component.For<IReadModelRepository>().ImplementedBy<ReadModelRepository>().LifeStyle.Singleton,
                    //Component.For<IBusEventHandler>().ImplementedBy<OrderCreatedEventHandler>()
                    //    .Named("OrderCreatedEventHandler").LifeStyle.Singleton,
                    //Component.For<IBusEventHandler>().ImplementedBy<OrderAddressChangedEventHandler>()
                    //    .Named("OrderAddressChangedEventHandler").LifeStyle.Singleton,                    
                    //Component.For<IBusEventHandler>().ImplementedBy<OrderDeletedEventHandler>()
                    //    .Named("OrderDeletedEventHandler").LifeStyle.Singleton,
                    Component.For<IKeyRepository<AggregateId>>().UsingFactoryMethod(
                        kernel =>
                        {
                            return new ArchiveRepository(kernel.Resolve<IKeyEventStoreFactory<AggregateId>>()
                                .Create(EventStoreConnection.Create(new IPEndPoint(IPAddress.Loopback, 1113))), null);
                            //return new DocumentRepository(kernel.Resolve<IKeyEventStore<DocumentId>>(), null);
                            //new CacheRepository(
                            //    new Repository(kernel.Resolve<IEventStore>(), kernel.Resolve<IEventPublisher>()),
                            //    kernel.Resolve<IEventStore>());
                        })

            );
        }
    }
}
