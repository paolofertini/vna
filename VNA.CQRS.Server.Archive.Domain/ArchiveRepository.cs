﻿using CQRSlite.Domain.Exception;
using CQRSlite.Domain.Factories;
using CQRSlite.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Aggregates;
using VNA.CQRS.Server.Archive.Domain.Infrastructure;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.Framework.Aggregates;
using VNA.CQRS.Server.Archive.Framework.Infrastructure;

namespace VNA.CQRS.Server.Archive.Domain
{
    public class ArchiveRepository : IKeyRepository<AggregateId>
    {
        private readonly IKeyEventStore<AggregateId> _eventStore;
        private readonly IEventPublisher _publisher;

        public ArchiveRepository(IKeyEventStore<AggregateId> eventStore, IEventPublisher publisher)
        {
            if (eventStore == null)
                throw new ArgumentNullException("eventStore");
            //if (publisher == null)
            //    throw new ArgumentNullException("publisher");

            _eventStore = eventStore;
            _publisher = publisher;
        }

        public T Get<T>(AggregateId aggregateId) where T : KeyAggregateRoot<AggregateId>
        {
            return LoadAggregate<T>(aggregateId);
        }

        public void Save<T>(T aggregate, long? expectedVersion = default(long?)) where T : KeyAggregateRoot<AggregateId>
        {
            if (expectedVersion != null && _eventStore.Get(
                    aggregate.Id, expectedVersion.Value).Any())
                throw new ConcurrencyException<AggregateId>(aggregate.Id);

            var i = 0;
            foreach (var @event in aggregate.GetUncommittedChanges())
            {
                if (@event.Id.IsEmpty())
                    @event.Id = aggregate.Id;
                if (@event.Id.IsEmpty())
                    throw new AggregateOrEventMissingIdException(
                        aggregate.GetType(), @event.GetType());

                @event.Version = expectedVersion.HasValue && expectedVersion.Value != -1 ? expectedVersion.Value + i++ : -1;
                @event.TimeStamp = DateTimeOffset.UtcNow;
                _eventStore.Save(@event);
                //_publisher.Publish(@event);
            }
            aggregate.MarkChangesAsCommitted();
        }

        private T LoadAggregate<T>(AggregateId id) where T : KeyAggregateRoot<AggregateId>
        {
            var aggregate = KeyAggregateFactory.CreateAggregate<T>();

            var events = _eventStore.Get(id, -1);
            if (!events.Any())
                throw new AggregateNotFoundException<AggregateId>(id);

            aggregate.LoadFromHistory(events);
            return aggregate;
        }
    }
}
