﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Domain.Values
{
    [DataContract]
    public class JSonObj
    {
        string _valueX;

        [DataMember]
        public string ValueX
        {
            get { return _valueX; }
            set { _valueX = value; }
        }
    }
}
