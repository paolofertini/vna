﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Domain.Values
{
    [DataContract]
    public class Header
    {
        [DataMember]
        public Patient Patient { get; set; }
        [DataMember]
        public DocumentInfo Info { get; set; }
    }
}
