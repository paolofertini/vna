﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Domain.Values
{
    public class Department
    {
        public string Name { get; set; }
        public string SendingApplication { get; set; }
    }
}
