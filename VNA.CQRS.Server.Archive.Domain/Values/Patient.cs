﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Domain.Values
{
    public class Patient
    {
        public enum Gender
        {
            Male,
            Female,
            Unkwnown
        }

        public string ID { get; set; }
        public string IDIssuer { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public Gender Sex { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
