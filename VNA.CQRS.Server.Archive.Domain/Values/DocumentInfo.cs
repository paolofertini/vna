﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Domain.Values
{
    public class DocumentInfo
    {
        public Department Department { get; set; }
        public DateTime Data { get; set; }        
        public string Type { get; set; }
    }
}
