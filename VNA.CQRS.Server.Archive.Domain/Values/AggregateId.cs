﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace VNA.CQRS.Server.Archive.Domain.Values
{
    [DataContract]
    public class AggregateId : IComparable, IComparable<AggregateId>, IEquatable<AggregateId>
    {
        [DataMember]
        public string Id { get; set; }

        public int CompareTo(AggregateId other)
        {
            return string.Format("{0}", this.Id).CompareTo(string.Format("{0}", other.Id, true));
        }

        public int CompareTo(object obj)
        {
            if (obj.GetType() != typeof(AggregateId))
                throw new NotImplementedException();
            else
                return CompareTo(obj as AggregateId);
        }

        public bool Equals(AggregateId other)
        {
            return (string.Compare(other.Id, Id, true) == 0);
        }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(Id);
        }

        public static bool operator == (AggregateId obj1, AggregateId obj2)
        {
            return obj1.Equals(obj2);
        }

        public static bool operator !=(AggregateId obj1, AggregateId obj2)
        {
            return !obj1.Equals(obj2);
        }
    }
}
