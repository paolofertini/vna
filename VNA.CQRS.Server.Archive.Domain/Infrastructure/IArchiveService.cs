﻿using CQRSlite.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Commands;
using VNA.CQRS.Server.Archive.Framework.Faults;
using VNA.CQRS.Server.Archive.Domain.Values;

namespace VNA.CQRS.Server.Archive.Domain.Infrastructure
{
    [ServiceContract]
    [StandardFaults]
    public interface IArchiveService
    {
        [OperationContract]
        Task<bool> SendCommandAsync(KeyCommand command);

    }
}

