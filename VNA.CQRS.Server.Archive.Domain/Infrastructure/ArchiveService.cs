﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Commands;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.ReadModel;

namespace VNA.CQRS.Server.Archive.Domain.Infrastructure
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class ArchiveService : IArchiveService
    {
        private readonly ArchiveCommandsHandlers commandHandlers;
        private readonly IReadModelRepository readModelRepository;

        public ArchiveService(ArchiveCommandsHandlers commandHandlers, IReadModelRepository readModelRepository)
        {
            this.commandHandlers = commandHandlers;
            this.readModelRepository = readModelRepository;
        }

        [FaultContract(typeof(NotImplementedException))]
        public async Task<bool> SendCommandAsync(KeyCommand command)
        {
            await Task.Run(() =>
            {
                Type[] tt = commandHandlers.GetType().GetGenericArguments();

                var meth = (from m in typeof(ArchiveCommandsHandlers)
                    .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                            let prms = m.GetParameters()
                            where prms.Count() == 1 && prms[0].ParameterType == command.GetType()
                            select m).FirstOrDefault();

                if (meth == null)
                    throw new Exception();

                    //throw new BusinessLogicException(
                    //    string.Format("Handler for {0} could not be found", command.GetType().Name));

                meth.Invoke(commandHandlers, new[] { command });
            });

            return true;
        }
    }
}
