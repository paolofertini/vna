﻿using CQRSlite.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.Domain.Aggregates;
using CQRSlite.Domain.Exception;
using VNA.CQRS.Server.Archive.Framework.Infrastructure;
using VNA.CQRS.Server.Archive.Framework.Aggregates;

namespace VNA.CQRS.Server.Archive.Domain.Infrastructure
{
    public class SessionTracking : IKeySession<AggregateId>
    {
        private readonly IKeyRepository<AggregateId> _repository;
        private readonly Dictionary<AggregateId, AggregateDescriptor> _trackedAggregates;

        public SessionTracking(IKeyRepository<AggregateId> repository)
        {
            if (repository == null)
                throw new ArgumentNullException("repository");

            _repository = repository;
            _trackedAggregates = new Dictionary<AggregateId, AggregateDescriptor>();
        }

        public void Add<T>(T aggregate) where T : KeyAggregateRoot<AggregateId>
        {
            if (!IsTracked(aggregate.Id))
                _trackedAggregates.Add(aggregate.Id,
                    new AggregateDescriptor
                    {
                        Aggregate = aggregate,
                        Version = aggregate.Version
                    });
            else if (_trackedAggregates[aggregate.Id].Aggregate != aggregate)
                throw new ConcurrencyException<AggregateId>(aggregate.Id);
        }

        public T Get<T>(AggregateId id, int? expectedVersion = null) where T : KeyAggregateRoot<AggregateId>
        {
            if (IsTracked(id))
            {
                var trackedAggregate = (T)_trackedAggregates[id].Aggregate;
                if (expectedVersion != null && trackedAggregate.Version != expectedVersion)
                    throw new ConcurrencyException<AggregateId>(trackedAggregate.Id);
                return trackedAggregate;
            }

            var aggregate = _repository.Get<T>(id);
            if (expectedVersion != null && aggregate.Version != expectedVersion)
                throw new ConcurrencyException<AggregateId>(id);
            Add(aggregate);

            return aggregate;
        }

        private bool IsTracked(AggregateId id)
        {
            return _trackedAggregates.ContainsKey(id);
        }

        public void Commit()
        {
            foreach (var descriptor in _trackedAggregates.Values)
            {
                _repository.Save(descriptor.Aggregate, descriptor.Version);
            }
            _trackedAggregates.Clear();
        }

        private class AggregateDescriptor
        {
            public KeyAggregateRoot<AggregateId> Aggregate { get; set; }
            public long Version { get; set; }
        }
    }
}
