﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Commands;
using VNA.CQRS.Server.Archive.Domain.Infrastructure;
using VNA.CQRS.Server.Archive.Domain.Values;

namespace VNA.CQRS.Server.Archive.Domain.Client
{
    public partial class ArchiveServiceClient :
        System.ServiceModel.ClientBase<IArchiveService>, IArchiveService
    {

        public ArchiveServiceClient()
        {
        }

        public ArchiveServiceClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public ArchiveServiceClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public ArchiveServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public ArchiveServiceClient(System.ServiceModel.Channels.Binding binding,
            System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        public Task<bool> SendCommandAsync(KeyCommand command)
        {
            return base.Channel.SendCommandAsync(command);
        }
    }
}
