﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Framework.Faults;
using VNA.CQRS.Server.Archive.Domain.Infrastructure;

namespace VNA.CQRS.Server.Archive.Domain.Client
{
    public class ArchiveServiceInvoker
    {
        private Binding _binding;
        private EndpointAddress _endPoint;

        //public ArchiveServiceInvoker(Binding binding, EndpointAddress endpoint)
        //{
        //    _binding = binding;
        //    _endPoint = endpoint;
        //}

        public TR CallService<TR>(Func<IArchiveService, TR> requestAction)
        {
            ArchiveServiceClient proxy = new ArchiveServiceClient();
            return requestAction(proxy);
            //try
            //{

            //}
            //catch (FaultException<GenericFault> gf)
            //{
            //    throw new ApplicationException("A FaultException<GenericFault> occured", gf.InnerException);
            //}
            //catch (FaultException<BusinessLogicFault> bf)
            //{
            //    throw new BusinessLogicException(bf.Message);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("A Exception occured", ex);
            //}
        }
    }
}
