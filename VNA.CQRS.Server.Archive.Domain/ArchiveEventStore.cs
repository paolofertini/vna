﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CQRSlite.Events;
using VNA.CQRS.Server.Archive.Domain.Events;
using VNA.CQRS.Server.Archive.Domain.Values;
using EventStore.ClientAPI;
using VNA.CQRS.Server.Archive.Framework.Infrastructure;
using VNA.CQRS.Server.Archive.Framework.Events;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json.Linq;

namespace VNA.CQRS.Server.Archive.Domain
{
    public class ArchiveEventStore : IKeyEventStore<AggregateId>
    {
        /// <summary>
        /// EventStore.ClusterNode.exe --db ./db --log ./logs
        /// </summary>
        private IEventStoreConnection _connection;

        public ArchiveEventStore(IEventStoreConnection connection)
        {
            _connection = connection;
            _connection.ConnectAsync().Wait();
        }

        private static DocumentCreatedEvent<AggregateId> AsDocumentCreatedEvent(byte[] arrBytes)
        {
            return JsonConvert.DeserializeObject<DocumentCreatedEvent<AggregateId>>(System.Text.Encoding.UTF8.GetString(arrBytes));
        }

        public IEnumerable<IKeyEvent<AggregateId>> Get(AggregateId aggregateId, long? fromVersion)
        {
            List<IKeyEvent<AggregateId>> events = new List<IKeyEvent<AggregateId>>();

            var streamEvents = new List<ResolvedEvent>();

            StreamEventsSlice currentSlice;
            long nextSliceStart = StreamPosition.Start;

            do
            {
                currentSlice = _connection.ReadStreamEventsForwardAsync("Document", nextSliceStart, 200, false).Result;

                nextSliceStart = currentSlice.NextEventNumber;

                streamEvents.AddRange(currentSlice.Events.Where(e =>
                                            {                                                
                                                if (e.Event.EventNumber > (!fromVersion.HasValue ? -1 : fromVersion.Value))
                                                {
                                                    var @event = AsDocumentCreatedEvent(e.Event.Data);
                                                    @event.Version = e.Event.EventNumber;

                                                    if (@event.Id == aggregateId) events.Add(@event);

                                                    return true;
                                                }

                                                return false;
                                            }
                ));
            } while (!currentSlice.IsEndOfStream);

            return events;
        }

        public void Save(IKeyEvent<AggregateId> @event)
        {
            if (@event.GetType() == typeof(DocumentCreatedEvent<AggregateId>))
            {
                DocumentCreatedEvent<AggregateId> doc_event = @event as DocumentCreatedEvent<AggregateId>;

                var myEvent = new EventData(Guid.NewGuid(), @event.GetType().Name, true,
                                Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(doc_event)),
                                Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(doc_event.Header)));

                _connection.AppendToStreamAsync("Document",  ExpectedVersion.Any, myEvent).Wait();
            }
        }
    }
}
