﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Host;

namespace VNA.CQRS.Server.Archive.Domain
{
    class Program
    {
        private static Logger logger = LogManager.GetLogger("Program");

        static void Main(string[] args)
        {
            try
            {
                logger.Info("Initialising VNA ArchiveService");
                ArchiveServiceRunner dealingServiceRunner = new ArchiveServiceRunner();
                dealingServiceRunner.Start();
                ManualResetEvent mre = new ManualResetEvent(false);
                Console.CancelKeyPress += (s, e) =>
                {
                    if (e.SpecialKey == ConsoleSpecialKey.ControlC)
                    {
                        mre.Set();
                    }
                };

                mre.WaitOne();
                dealingServiceRunner.Stop();
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }
    }
}
