﻿using CQRSlite.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VNA.CQRS.Server.Archive.Domain.Commands;
using VNA.CQRS.Server.Archive.Domain.Values;
using VNA.CQRS.Server.Archive.Framework.Events;

namespace VNA.CQRS.Server.Archive.Domain.Events
{
    public abstract class EventBase<T> : IKeyEvent<T>
    {
        public T Id { get; set; }
        public long Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }

        public Header Header { get; set;  }
        public JObject Metadata { get; set; }
    }

    public class DocumentCreatedEvent<T> : EventBase<T>
    {
        public DocumentCreatedEvent(T id, Header header, JObject metadata)
        {
            base.Id = id;
            base.Header = header;
            base.Metadata = metadata;
            base.Version = -1;
        }
    }
}
